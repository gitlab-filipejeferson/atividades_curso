package AtividadeModulo03;

import java.util.ArrayList;
import java.util.Scanner;

public class AtividadeModuloDois {

    public static void main(String[] args) {

        int opcaoMenu = 0, menu = 0;
        Scanner input = new Scanner(System.in);

        do {
            System.out.println("-------   MENU  -----------\n" +
                    "|     1 - CALCULAR IMC             |\n" +
                    "|     2 - LISTAR IMC               |\n" +
                    "|     3 - PESQUISAR IMC            |\n" +
                    "|     4 - EXCLUIR IMC              |\n\n" +
                    "Informe uma opção: ");
            opcaoMenu = Integer.parseInt(input.nextLine());

            switch (opcaoMenu) {
                case 1:
                    ArrayList<CalculoIMC> calIMC = new ArrayList<CalculoIMC>();
                    CalculoIMC calcularIMC = new CalculoIMC();
                    CalculoIMC imcPessoa = new CalculoIMC();

                    System.out.println("\nVocê escolheu calcular seu IMC! \n");

                    calcularIMC.CalculoIMC();

                    System.out.println("\ndigite 0 para voltar ao menu ou 1 para encerrar!");
                    menu = Integer.parseInt(input.nextLine());
                    break;
                case 2:
                    System.out.println("\nVocê escolheu listar IMC calculado\n" +
                            "\ndigite 0 para voltar ao menu ou 1 para encerrar!");
                    menu = Integer.parseInt(input.nextLine());
                    break;
                case 3:
                    System.out.println("Você escolheu pesquisar IMC calculado\n" +
                            "\ndigite 0 para voltar ao menu ou 1 para encerrar!");
                    menu = Integer.parseInt(input.nextLine());
                    break;
                case 4:
                    System.out.println("Você escolheu excluir IMC calculado\n" +
                            "\ndigite 0 para voltar ao menu ou 1 para encerrar!");
                    menu = Integer.parseInt(input.nextLine());
                    break;

                default:
                    System.out.println("Opção Inválida!\n" +
                            " retorne ao menu e escolha uma opção válida ou encerre a operação.\n" +
                            "\ndigite 0 para voltar ao menu ou 1 para encerrar!");
                    menu = Integer.parseInt(input.nextLine());

            }
        }while (menu == 0);
        System.out.println("Programa Encerrado!");
        }


}