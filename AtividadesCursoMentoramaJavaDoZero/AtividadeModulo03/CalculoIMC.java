package AtividadeModulo03;

import java.util.ArrayList;
import java.util.Scanner;

public class CalculoIMC extends IMC{

    Scanner input = new Scanner(System.in);
    public CalculoIMC(double peso, double altura, double imc) {
        super(peso, altura, imc);
    }

    public CalculoIMC() {

    }


    public void CalculoIMC() {

        ArrayList<CalculoIMC> calIMC = new ArrayList<CalculoIMC>();
        CalculoIMC imcPessoa = new CalculoIMC();

        System.out.println("Informe seu Peso");
        imcPessoa.setPeso(input.nextDouble());
        System.out.println("Informe a sua altura");
        imcPessoa.setAltura(input.nextDouble());

        double imcFinal = imcPessoa.getPeso()/(imcPessoa.getAltura()*imcPessoa.getAltura());
        System.out.printf("O seu IMC é de: %.2f ", imcFinal);
        calIMC.add(imcPessoa);


    }

}
