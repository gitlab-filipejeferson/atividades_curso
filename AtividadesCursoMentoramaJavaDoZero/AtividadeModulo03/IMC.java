package AtividadeModulo03;

public class IMC {
    private double peso;
    private double altura;
    private double imc;

    public  IMC(double peso, double altura, double imc){
        this.peso = peso;
        this.altura = altura;
        this.imc = imc;

    }

    public  IMC() {

    }

    public double getPeso() {
        return peso;
    }

    public IMC setPeso(double peso) {
        this.peso = peso;
        return this;
    }

    public double getImc() {
        return imc;
    }

    public IMC setImc(double imc) {
        this.imc = imc;
        return this;
    }

    public double getAltura() {
        return altura;
    }

    public IMC setAltura(double altura) {
        this.altura = altura;
        return this;
    }
}
