package AtividadeModulo04;

public class Carro {

    private String cor = "Vermelha";

    final int velocidadeMax;
    int velocidadeAtual;
    private int aumentoVelocidade = 5;

    protected Carro(int velocidade_maxima) {
        velocidadeMax = velocidade_maxima;
    }

    void acelerar() {
        if (velocidadeAtual + getAumentoVelocidade() > velocidadeMax) {
            velocidadeAtual = velocidadeMax;
        } else {
            velocidadeAtual += getAumentoVelocidade();
        }
    }

    void frear(){
        if(velocidadeAtual >= getAumentoVelocidade()) {
            velocidadeAtual -= getAumentoVelocidade();
        }else{
                    velocidadeAtual = 0;
                }
            }

    @Override
    public String toString() {
        return "\nVelocidade atual da Ferrari " + getCor() + " é de " + velocidadeAtual + " km/h.";
    }

    public int getAumentoVelocidade() {
        return aumentoVelocidade;
    }

    public void setAumentoVelocidade(int aumentoVelocidade) {
        this.aumentoVelocidade = aumentoVelocidade;
    }

    public String getCor() {
        return cor;
    }

    public void setCor(String cor) {
        this.cor = cor;
    }
}

