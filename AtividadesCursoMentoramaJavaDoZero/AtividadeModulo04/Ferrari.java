package AtividadeModulo04;

public class Ferrari extends Carro implements Esportivo, Luxo{


    private boolean ligarTurbo;
    private boolean ligarArCondicionado;

    Ferrari(){
        this(300);
    }

    public Ferrari(int velocidade_maxima) {
        super(velocidade_maxima);
        setAumentoVelocidade(15);
    }

    @Override
    public void acionarTurbo() {
       ligarTurbo = true;
    }

    @Override
    public void desacionarTurbo() {
        ligarTurbo = false;
    }

    @Override
    public void ligarArCondicionado() {
        ligarArCondicionado = true;
    }

    @Override
    public void desligarArCondicionado() {
        ligarArCondicionado = false;
    }

    @Override
    public int getAumentoVelocidade() {
        if(ligarTurbo && !ligarArCondicionado){
            return 35;
        }else if(ligarTurbo && ligarArCondicionado){
            return 30;

        }else if(!ligarTurbo && !ligarArCondicionado){
            return 20;
        }else{
            return 15;
        }

    }


}
