package AtividadeModulo04;

public class TestarCarro {

    public static void main(String[] args) {

        Ferrari ferrari = new Ferrari();

        ferrari.acelerar();
        ferrari.desligarArCondicionado();
        ferrari.acionarTurbo();
        ferrari.acelerar();
        ferrari.acelerar();
        System.out.println(ferrari.toString());
    }
}
