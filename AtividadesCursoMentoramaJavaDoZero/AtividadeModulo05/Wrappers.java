package AtividadeModulo05;

import java.util.Scanner;

public class Wrappers {

    public static void main(String[] args) {

        int qtdeNotas = 0, n = 0, a = 0, f = 0;
        double total = 0;

        Scanner input = new Scanner(System.in);

        System.out.println("informe a quantidade de notas do aluno:");
        qtdeNotas = input.nextInt();

        Double notas[] = new Double[qtdeNotas];

        for(int i = 0; i < notas.length; i++) {
            System.out.println("Informe a " + (n + 1) + "º nota: ");
            notas[i] = input.nextDouble();
            total+=notas[i];
            n++;
        }

        System.out.println("\n  =========   NOTAS CADASTRADAS   ==========\n");
        for(double nota: notas) {
            System.out.println((f + 1) + "º nota........" + nota);
            a++;
            f++;
        }
        System.out.println("\nTotal de notas calculadas: " + notas.length);

        double media = total/qtdeNotas;

        System.out.printf("\nA sua média final é de %.2f", media);
        if((media > 0) && (media <=5)){
            System.out.println("\n...Você está reprovado!.......");
        }else if((media > 5) && (media < 7)){
            System.out.println("\n...Você está em recuperação!.......");
        }else if((media > 7) && (media <= 10)){
            System.out.println("\n...Parabéns você está aprovado!.......");
        }

        System.out.println("\n--------------------------------------------------------");
        input.close();

    }
}
